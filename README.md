# README #

## **Description** ##
Load a full isolated lampstack, optimised for development using Zurb foundation and configured with Ruby, NodeJS, Postgres and MySQL, within VirtualBox.

## **Key features** ##

* Apache2
* PHP5.5
* GIT
* Ruby
* NodeJS
* Chef
* Ruby gems
* Bundler 
* Homebrew
* Rails
* Sass
* Compass
* Cyaml
* VIM
* Modified vhosts and port forwarding

## **Databases** ##
* MySQL (default: root, root)
* PHPmyAdmin (default: root, root)
* PostgreSQL (default: root, root)

## **Installed Framworks** ##
* An automatic site overview page - at sites.dev
* Automatic installs of zurb Foundation with one line.
* The PhpMVC framework
* Sub app installation within PhpMVC
* Automatic Database Table scripting

## *Installation* ##
[Install vagrant](https://www.vagrantup.com/docs/installation/)

[Install virtualbox](https://www.virtualbox.org/wiki/Downloads)

Ensure the host is connected to git ssh. E.g.
```
#!shell
ssh -T git@bitbucket.org
```

Clone LampStack and boot up the vagrant box, named lamp.
```
#!shell
git clone git@bitbucket.org:simeonw1/lampstack.git file/root
cd file/root
vagrant up
```

## Foundation ##
* A blank new foundation project is installed at file/root/foundationproject
* Foundation can also be embedded within PhpMVC at file/root/phpmvc/public/foundation (or /var/www/phpmvc...), and in individual apps at file/root/phpmvc/apps/appname/public/foundation (or /var/www/phpmvc/apps/appname/...)
```
New projects:
#!shell
cd file/root/path/to/foundation
foundation new YOUR_APP
#or
compass create <project-name> -r ZURB-foundation --using ZURB-foundation --force
#Or
compass create path/to/<project-name> -r ZURB-foundation --using ZURB-foundation --force
```

Add to existing:
#!shell
cd file/root/path/to/foundation
compass install ZURB-foundation/project
```

More information [here](http://thesassway.com/projects/zurb-foundation)

## Port Forwarding ##
* file/root syncs to /var/www/ on the guest - accessed through root.dev or 127.0.0.1
* file/root/sites page is accessed through home.dev, sites.dev or local.dev
* foundation.dev syncs with the default foundation project /var/www/foundationproject
* file/root/projectfolder syncs to var/www/projectfolder 
* Access via projectfolder.dev, projectfolder.sites.dev or /projectfolder


## Configuration: ##
Edit config environment by:

```
#!shell

sh "file/root/vagrant/set-env.sh" /var/www/$1 d #development
sh "file/root/vagrant/set-env.sh" /var/www/$1 i #installation
sh "file/root/vagrant/set-env.sh" /var/www/$1 p #production
```