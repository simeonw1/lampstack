#!/bin/bash
#Load the functions
source /vagrant/vagrant/functions.sh

echo "---- Set up PHP-MVC ---"

project=phpmvc

#git pull https://github.com/simeonw/php-mvc.git /var/www/html/mvc  
git_get git@bitbucket.org:simeonw1/php-mvc.git "$project" config
#3=""

echo "---- Adding Apps ---"
######## EDIT HERE #################
apps=("localcinema" "testApp" "fatigue" "coffee" "mail")
######## EDIT HERE #################
#
for i in "${apps[@]}" ; do
  #echo "Installing $i"
  # e.g. git_get git@bitbucket.org:simeonw1/localcinema.git /var/www/html/mvc/apps/localcinema
  git_get git@bitbucket.org:simeonw1/"$i".git "$project"/apps/"$i"
done

#
echo "########################################################"
echo "#------------------------------------------------------#"
echo "#---- Installed phpmvc --------------------------------#"
echo "#------------------ with a range of apps---------------#"
echo "#------------------------------------------------------#"
echo "#------------------------------------------------------#"
echo "########################################################"
