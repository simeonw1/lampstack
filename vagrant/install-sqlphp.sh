#!/usr/bin/env bash

echo "--- Installing base packages ---"
sudo apt-get install -y vim curl python-software-properties

#echo "--- Installing PHP5 ---"
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get install -y php5 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt php5-imap #php5-mysql #mysql-server-5.5  

# Use single quotes instead of double quotes to make it work with special-character passwords
PASSWORD='root' # username is also root

 echo "--- Installing mysql ---"
# # install mysql and give password to installer
 sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
 sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
 sudo apt-get -y install mysql-server
 sudo apt-get install php5-mysql

# install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin

#gksu gedit /etc/apache2/apache2.conf
#sudo ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf
#sudo a2enconf phpmyadmin

echo "--- Installing and configuring Xdebug ---"
sudo apt-get install -y php5-xdebug
#find the location: #sudo find / -name 'xdebug.so'
cat << EOF | sudo tee -a #/etc/php5/mods-available/xdebug.ini
xdebug.scream=1
xdebug.cli_color=1
xdebug.show_local_vars=1
EOF

echo "--- Updating packages list ---"
sudo apt-get update

echo "--- Turn errors on. ---"
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

echo "--- Set PHP short open tags to on. ---"
sudo sed -i "s/short_open_tag = .*/short_open_tag = On/" /etc/php5/apache2/php.ini

echo "--- Restarting Apache ---"
sudo service apache2 restart


echo "########################################################"
echo "#------------------------------------------------------#"
echo "#-----------------L-A-M-P S-T-A-C-K--------------------#"
echo "#------------------------------------------------------#"
echo "#---- sites.dev home.dev resolves 127.0.0.1/sites -----#"
echo "#---- 127.0.0.1 accesses root directory at /projects --#"
echo "#---- sub.sites.dev resolves to 127.0.0.1/sub ---------#"
echo "#---- sites.dev = overview page------------------------#"
echo "#---- ./projects points to /var/log/html   ------------#"
echo "#------192.168.33.10-----------------------------------#"
echo "#------ mysql is installed with password root  --------#"
echo "#------------------------------------------------------#"
echo "#------------------------------------------------------#"
echo "#------------------------------------------------------#"
echo "########################################################"