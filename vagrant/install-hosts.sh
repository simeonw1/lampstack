#!/usr/bin/env bash

echo "--- Configuring Hosts ---"
sudo a2enmod vhost_alias

echo "---Set up home.dev and foldername.dev ---"
 # setup hosts file
VHOST=$(cat <<EOF

<VirtualHost *:80>
  ServerAlias root.dev
    VirtualDocumentRoot /var/www/html
    UseCanonicalName Off
</VirtualHost>

<VirtualHost *:80>
  ServerAlias sites.dev home.dev local.dev
    VirtualDocumentRoot /var/www/html/sites
    UseCanonicalName Off
</VirtualHost>

<VirtualHost *:80>
  ServerAlias *.sites.dev *.local.dev *.home.dev
    VirtualDocumentRoot /var/www/html/%-3
    UseCanonicalName Off
</VirtualHost>

 <VirtualHost *:80>
  ServerAlias *.dev
     VirtualDocumentRoot /var/www/html/%-2
    UseCanonicalName Off
 </VirtualHost>

  <Virtualhost *:80>
    VirtualDocumentRoot "/var/www/html/%-7+"
    ServerName xip
    ServerAlias *.xip.io
    UseCanonicalName Off
  </Virtualhost>

# <VirtualHost *:3000>
#   ServerAlias *.dev
#     VirtualDocumentRoot /var/www/html/
#     UseCanonicalName Off
# </VirtualHost>

EOF
)

echo "${VHOST}" |  sudo tee -a /etc/apache2/sites-available/000-default.conf > /dev/null

#echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

sudo service apache2 restart
echo "--- Enabling mod-rewrite for htaccess rewrite engine ---"
sudo a2enmod rewrite
sudo sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

echo "--- Fixing the TTY STDIN error ---"
sudo sed -i 's/^mesg n$/tty -s \&\& mesg n/g' /root/.profile

echo "--- Restarting Apache ---"
sudo service apache2 restart


echo "########################################################"
echo "#------------------------------------------------------#"
echo "#-----------------L-A-M-P S-T-A-C-K--------------------#"
echo "#------------------------------------------------------#"
echo "#---- sites.dev home.dev resolves 127.0.0.1/sites -----#"
echo "#---- 127.0.0.1 accesses root directory at /projects --#"
echo "#---- sub.sites.dev resolves to 127.0.0.1/sub ---------#"
echo "#---- sites.dev = overview page------------------------#"
echo "#---- ./projects points to /var/log/html   ------------#"
echo "#------192.168.33.10-----------------------------------#"
echo "#------ mysql is installed with password root  --------#"
echo "#------------------------------------------------------#"
echo "#------------------------------------------------------#"
echo "#------------------------------------------------------#"
echo "########################################################"