# -*- mode: ruby -*-
# vi: set ft=ruby :

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
fi

#$1 #directory without config.php
#trim slash from directory without config file
DIRECTORY=${1%/}
# Env to set to - options are install, production, dev
SETENV=$2

if [ -d "$DIRECTORY" ]; then

FILE="$DIRECTORY/config.php"

	#
	#$1 = FILE
	#$2 = string to replace.
	#
	replace_string() {
	    local search=''\''ENVIRONMENT'\'','\''.*'\'''
	    local replace=''\''ENVIRONMENT'\'','\''$2'\'''

		#Setting environment to $2
		grep ${search} -q $1 && echo "" || echo "Error: No environment to specify."
		#This works if term exists - don't change!
		sed -i.tmp 's/'\''ENVIRONMENT'\'','\''.*'\''/'\''ENVIRONMENT'\'','\'''$2''\''/g' $1
		#sed -i '/installation/{s//production/;h};${x;/./{x;q0};x;q1}' $1

		grep ''\''ENVIRONMENT'\'','\'''$2''\''' -q $1 && echo "$2 config successful" || echo "Config change unsuccessful"
	}

	if [ -f "$FILE" ];
	then
	   #echo "$FILE exists."
	    if [ "$2" = "install" ] || [ "$2" = "i" ] && [ -n "$2" ] ; then
		  # config.vm.provision "shell", inline: <<-SHELL	
			replace_string $FILE installation
		  # SHELL
		elif [ "$2" = "production" ] || [ "$2" = "p" ]; then
			replace_string $FILE production
		else 
			replace_string $FILE development
		     # sed -i '/production/{s//development/;h};${x;/./{x;q0};x;q1}' $FILE
		     # sed -i '/development/{s//development/;h};${x;/./{x;q0};x;q1}' $FILE
		fi 
	else
	   echo "$FILE does not exist" >&2
	fi
else
	  # $DIRECTORY doesn't exist.
	   echo "Directory $DIRECTORY does not exist."
fi


