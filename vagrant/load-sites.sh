#!/bin/bash
#Load the functions
source /vagrant/vagrant/functions.sh

echo "---- Install the Local Home Page from Git ---"

git_get git@github.com:simeonw/LocalHomePage.git sites  
#git pull https://github.com/simeonw/LocalHomePage /var/www/html/sites  

echo "########################################################"
echo "#------------------------------------------------------#"
echo "#---- Installed the sites page ------------------------#"
echo "#------------------------------------------------------#"
echo "########################################################"
