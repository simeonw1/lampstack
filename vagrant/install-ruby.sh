 #!/usr/bin/env bash

 source $HOME/.rvm/scripts/rvm

 rvm use --default --install $1

 shift

 if (( $# ))
 then gem install $@
 fi

 rvm cleanup all


# source /home/vagrant/.rvm/scripts/rvm #/usr/local/rvm/scripts/rvm
# rvm requirements
# rvm install ruby
# rvm use ruby --default
# rvm rubygems current

# echo "RBENV Install"
# git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
#   echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
#   echo 'eval "$(rbenv init -)"'               >> ~/.bashrc
#   source ~/.bashrc

# echo "Installing Ruby"
#   git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
#   sudo -H -u vagrant bash -i -c 'rbenv install 2.1.3'
#   sudo -H -u vagrant bash -i -c 'rbenv rehash'
#   sudo -H -u vagrant bash -i -c 'rbenv global 2.1.3'
#   sudo -H -u vagrant bash -i -c 'gem install bundler --no-ri --no-rdoc'
#   sudo -H -u vagrant bash -i -c 'rbenv rehash'
#   # sudo -u postgres createdb --locale en_US.utf8 --encoding UTF8 --template template0 development
#   # echo "ALTER USER postgres WITH PASSWORD \'root\';" | sudo -u postgres psql
