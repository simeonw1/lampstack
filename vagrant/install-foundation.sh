#!/usr/bin/env bash
echo "--- Install Foundation Project ---"

foundation_info() {


echo "########################################################"
echo "#------------------------------------------------------#"
echo "#-----------------FOUNDATION CONFIG--------------------#"
echo "#------------------------------------------------------#"
echo "#-----Load througg vagrantfile-------------------------#"
echo "#------------------------------------------------------#"
echo "#---- foundation new <dir> to start a project ---------#"
echo "#---- cd <dir> ----------------------------------------#"
echo "#---- bundle [to compile compass/bower etc] -----------#"
echo "#-----Then Run: bundle exec compass watch to compile---#"
echo "#------------------------------------------------------#"
echo "#----------------cd projectfolder ---------------------#"
echo "#----------------bundle--------------------------------#"
echo "#----------------bundle exec compass watch-------------#"
echo "#------------------------------------------------------#"
echo "#------------------------------------------------------#"
echo "#---Needs rvm, ruby and node (which gives bower/grunt--#"
echo "#------------------------------------------------------#"
echo "########################################################"

}

if [ $# -eq 0 ]
  then
    echo "No foundation project name supplied"
    echo " "
    foundation_info
    exit 1
fi


project=/var/www/html/$1

foundation new $project #--libsass without = compass
cd $project
bundle


#foundation new /var/www/phpmvc/public/foundation --libsass 


foundation_info