#!/bin/bash
# Function to load the site and install from config.
# $1 = foldername e.g. path to site, after the root.
# 
install_tables() {
    # load the page to install the site.
    #wget http://local.dev/$1 > /dev/null
    
    # sed -i 's/installation/development/g' /var/www/$1/config.php 
    #with error checking
    #sed -i '/installation/{s//development/;h};${x;/./{x;q0};x;q1}' /var/www/$1/config.php   
      
    #ls "/vagrant/vagrant"
    sudo sh "/vagrant/vagrant/set-env.sh" /var/www/$1 i

    #echo "Loading the tables"
    wget -O - -q "http://$1.dev" >> /var/www/$1/log.html #> /dev/null # mail -s "Subject goes here" to@example.com
    
    sudo sh "/vagrant/vagrant/set-env.sh" /var/www/$1 d
    #sh vagrant/set-env.sh projects/phpmvc/ p # production
}

# Function to pull if exists; clone if it doesn't.
# Take $1 = git ssh url
# $2 = local project name e.g. phpmvc
# $3 = config, if import to folder $2 required 
git_get(){
    if cd /var/www/$2; then 
        echo "pulling $2 into /var/www/$2"
        git pull; 
    else 
        echo "cloning $2 into /var/www/$2"
        git clone $1 /var/www/$2; 
    fi
    if [ -v $3 ]; then
        #echo "Not importing config in /var/www/$2"
        echo $3
    else
        echo "$3 set. Importing config in /var/www/$2"
        # import the config file - to base, or override in folder.
        #e.g. cp /vagrant/config-sample.php /var/www/html/mvc/config.php 
        cp /vagrant/config-sample.php /var/www/$2/config.php
        install_tables $2
    fi
}


# load_terminal() {

# osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' 
# -e 'tell application "Terminal" to do script "echo $1" in selected tab of the front window'

#   if [ $# -eq 0 ]
#     then
#       osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' 
#     else

#       osascript -e 'tell application "Terminal" to activate' -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' 
# -e 'tell application "Terminal" to do script "echo $1" in selected tab of the front window'

      
#   fi

# # tab="--tab"
# # cmd="bash -c 'echo "hello"';bash"
# # foo=""

# #   for i in 1 2; do
# #         foo+=($tab -e "$cmd")         
# #   done

# # gnome-terminal "${foo[@]}"

# # exit 0
# }