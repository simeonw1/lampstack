#!/usr/bin/env bash
# #Load the functions
# source functions.sh
# sudo apt-get update

rails_info() {


echo "########################################################"
echo "#------------------------------------------------------#"
echo "#-----------------RAILS CONFIG-------------------------#"
echo "#------------------------------------------------------#"
echo "#-----Loaded througg vagrantfile-----------------------#"
echo "#-----Just cd into dir and run rails server------------#"
echo "#------------------------------------------------------#"
echo "#---- rails new /var/www/html/<project> ---------------#"
echo "#---- cd /var/www/html/<project>-----------------------#"
echo "#---- bundle install ----------------------------------#"
echo "#-----Rails s -----------------------------------------#"
echo "#------------------------------------------------------#"
echo "#-----Needs rvm, ruby and node and rails --------------#"
echo "#------------------------------------------------------#"
echo "########################################################"

}

gem install rails --no-ri --no-rdoc

if [ $# -eq 0 ]
  then
    echo "No rails project name supplied"
    echo " "
    rails_info
    exit 1
fi


project=/var/www/html/$1

##############################$#$
###########SSH Instructions######
################################$
# rails new /var/www/html/<project>
# cd /var/www/html/<project>
# bundle install
# rails s
# [Need to fix forwarding on port 3000]
################################$
################################$
################################$

rails new $project #--libsass without = compass
cd $project
bundle install


#foundation new /var/www/phpmvc/public/foundation --libsass 


rails_info

# Installing Ruby Build
# echo "---- Install Ruby  ---"
# sudo apt-get -y install ruby-full
# sudo apt-get -y install autoconf bison libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev

# echo "---- Install Compass ---"
# sudo apt-get -y install ruby-compass

# echo "--- Install Make ---" #, Python3 and g++
# sudo apt-get install -y make # python3 g++ 

# echo "--- Update NODE and NPM ---"
# # sudo chown -R $USER /usr/local

# #sudo npm cache clean -f
# # Install NodeJS
# # curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
# # apt-get install -y nodejs #sudo


# echo 'export PATH=$HOME/local/bin:$PATH' >> ~/.bashrc
# . ~/.bashrc
# mkdir ~/local
# mkdir ~/node-latest-install
# cd ~/node-latest-install
# curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
# ./configure --prefix=$HOME/local
# make install
# curl -L https://www.npmjs.com/install.sh | sh

# Install build essential - currently through chef
#sudo apt-get install -y build-essential

# Update the NPM version contained within NodeJS
# npm install -g npm@latest

#echo "--- Install grunt bower and jQuery ---"
# #sudo chown -R $(whoami) $HOME/.npm

# npm install -g bower #grunt-cli --allow-root
# npm install -g grunt-cli 
# npm install -g bower

# npm install -g gulp

# #Initiate Grunt in a folder by typing grunt-init
# #npm install -g grunt-init

# bower install jQuery

# echo "--- Fix /var/lib/gems permissions to enable bundler !!! Replace with RVM or RBENV ---"
# echo "Add and join 'gems' group."
# groupadd -f gems
# sudo usermod -aG gems $(whoami)
# # Reenter session.
# sudo -u $(whoami) bash
# # Allow write
# chown :gems /var/lib/gems/
# chmod g+sw /var/lib/gems/

# echo "--- Installing Gems (Sass, Compass and Foundation CLI) ---"
#sudo gem update --system
#xcode-select --install

#gem install cyaml
# gem install sass
#gem install compass duplicate above
# gem install ZURB-foundation
#gem uninstall foundation #update version

#npm install -g foundation-cli


# echo "--- Upgrade packages ---"

# apt-get -y upgrade
