#!/usr/bin/env bash

#export DEBIAN_FRONTEND=noninteractive
echo "Updating packages"
sudo apt-get update

echo "Apache2"
sudo apt-get install -y apache2 

echo "Build Essential"
sudo apt-get install build-essential -q -y

echo "Curl git and wget"
# install curl without prompts
sudo apt-get install curl -q -y

# install git
sudo apt-get install git-core wget -q -y

# Install sqllite3 dev - needed on ubuntu systems, e.g. for ruby compatibility
sudo apt-get install libsqlite3-dev

echo "---- Install Git and Set SSH Keys ---"

####################################################################
##
## //Generate a new key
## ssh-keygen -t rsa -b 4096 -C "simeonw@github.com"
## //Enable the ssh-agent
## eval "$(ssh-agent -s)" 
## //Add the generated ssh key to the host machine's ssh-agent 
## ssh-add ~/.ssh/id_rsa 
## // Copy ssh key to clipboard & manually add to bitbucket/github
## pbcopy < ~/.ssh/id_rsa.pub 
##
####################################################################

## MANNUALLY to enter password by typing: ssh -T git@github.com

    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    ssh-keyscan -H github.com >> ~/.ssh/known_hosts
    ssh -T git@github.com
    ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts
    ssh -T git@bitbucket.org
#    git clone git@github.com:you/your-private-repo
#    git clone git@bitbucket.org:simeonw1/***.git /var/www/html/mvc/apps/***