#!/usr/bin/env bash

# apt-key adv --keyserver keyserver.ubuntu.com --recv 68576280
# apt-add-repository "deb https://deb.nodesource.com/node_5.x $(lsb_release -sc) main"
# apt-get update
# apt-get install nodejs

echo "!------Install Node - and also NPM---------!";
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install -y  nodejs

echo "!------Install bower and grunt cli---------!";
sudo npm install -g bower grunt-cli

# echo "NodeJS"

# install nodejs and npm from source 
# (apt-get fetches a version too old to use with bower)
# required for bower and grunt-cli
# echo 'export PATH=$HOME/local/bin:$PATH' >> /home/vagrant/.profile
# source /home/vagrant/.profile
# mkdir -p /home/vagrant/local
# mkdir -p /home/vagrant/node-latest-install
# cd /home/vagrant/node-latest-install
# curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
# ./configure --prefix=/home/vagrant/local
# make install
# curl https://npmjs.org/install.sh | sh