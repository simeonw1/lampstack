# -*- mode: ruby -*-
# vi: set ft=ruby :

# Install vagrant plugins
required_plugins = %w(vagrant-vbguest)  # vagrant-gatling-rsync vagrant-librarian-chef-nochef vagrant-hostmanager vagrant-share

plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
if not plugins_to_install.empty?
  puts "Installing plugins: #{plugins_to_install.join(' ')}"
  if system "vagrant plugin install #{plugins_to_install.join(' ')}"
    exec "vagrant #{ARGV.join(' ')}"
  else
    abort "Installation of one or more plugins has failed. Aborting."
  end
end

# puts "Configuring SSH"
# exec "ssh -T git@github.com"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"
  # config.vm.box_url = "http://files.vagrantup.com/precise32.box"
  config.vm.provider :virtualbox do |vb|
      vb.name = "lamp-foundation"
  end

  # Enable SSH connection agent forwarding (default = false).
  config.ssh.forward_agent = true

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network :forwarded_port, guest: 80, host: 8080, auto_correct: true
  config.vm.network :forwarded_port, guest: 443, host: 8443, auto_correct: true  
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  config.vm.network "forwarded_port", guest: 8000, host: 8000 

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder ".", "/vagrant"
  config.vm.synced_folder "./projects", "/var/www/html" #, type: "rsync"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
    #export DEBIAN_FRONTEND=noninteractive
  # config.vm.provision "shell", inline: <<-SHELL
  #   sudo apt-get update
  #   sudo apt-get install -y apache2 
  # SHELL

  # Provision
  # upgrade linux and fetch utils for installing other things
  config.vm.provision :shell, path: "vagrant/install-linux-utils.sh"
  config.vm.provision :shell, privileged: false, path: 'vagrant/install-hosts.sh'
  config.vm.provision :shell, privileged: false, path: 'vagrant/install-sqlphp.sh'
  #
  #https://rvm.io/integration/vagrant :: ruby = version, then gems.
  config.vm.provision :shell, path: "vagrant/install-rvm.sh", args: "stable", privileged: false
  config.vm.provision :shell, path: "vagrant/install-ruby.sh", args: "1.9.3", privileged: false
  config.vm.provision :shell, path: "vagrant/install-ruby.sh", args: "2.0.0 haml nokogiri requirements compass bundler grunt bower rubies sass rake foundation foundation-sass", privileged: false
  config.vm.provision :shell, privileged: false, path: 'vagrant/install-node.sh'

  # Set up overall sites page
  config.vm.provision :shell, path: 'vagrant/load-sites.sh'

  # Install phpmvc with a number of apps.
  config.vm.provision :shell, path: 'vagrant/load-phpmvc.sh'

  # Uncomment to install foundation in projectname
  config.vm.provision :shell, privileged: false, path: 'vagrant/install-foundation.sh', args: "foundation"

  # Install rails server, and set up a project in projectname
  #config.vm.provision :shell, privileged: false, path: 'vagrant/install-rails.sh', args: "rails"

  #config.vm.provision :shell, privileged: false, path: 'vagrant/install-mongo.sh'
  #config.vm.provision :shell, privileged: false, path: 'vagrant/install-postgres.sh'



    # ######################################################
    # #Use Chef Solo to provision our virtual machine
    #   config.vm.provision :chef_solo do |chef|
    #     chef.cookbooks_path = ["cookbooks", "site-cookbooks"]
    #     chef.add_recipe "apt"
    #     #chef.add_recipe "nodejs"
    #     #chef.add_recipe "nodejs::npm"
    #     #chef.add_recipe "ruby_build"
    #     #chef.add_recipe "rbenv::user"
    #     #chef.add_recipe "rbenv::vagrant"
    #     #chef.add_recipe "vim"
    #     chef.add_recipe "postgresql::server"
    #     chef.add_recipe "postgresql::client"
    #     # Install Ruby 2.2.1 and Bundler
    #     # Set an empty root password for MySQL to make things simple
    #     chef.json = {
    #       # rbenv: {
    #       #   user_installs: [{
    #       #     user: 'vagrant',
    #       #     rubies: ["2.2.1"],
    #       #     global: "2.2.1",
    #       #     gems: {
    #       #       "2.2.1" => [
    #       #         { 'name' => "bundler",
    #       #           'version' => '1.1.rc.5' 
    #       #           },
    #       #         { 'name'    => 'rake' }
    #       #       ]
    #       #     }
    #       #   }]
    #       # },
    #         postgresql: {
    #         :version => "9.3",
    #           :password => {
    #           :postgres => "root"
    #           }
    #         }
    #     }
    #    end
    #     # config.vm.provision "docker" do |d|
    #     # d.pull_images "redis"
    #     # d.run "redis"
    #     # end
    #     # end

end
