<?php



//PATHS
//Always provide a trailing slash (/) after a path
//$host = "http://".$_SERVER['HTTP_HOST'];

define('URL', "http://".basename(__DIR__).".sites.dev/");
define('ROOT', dirname(__FILE__).'/'); #"/var/www/html/mvc/"

define('THEME', 'public/images/theme/');
define('SITETITLE', 'MY SITE TITLE');
define('SUBTITLE', 'SUB TITLE');
define('LIBS', 'libs/');
define('APPS', 'apps/');

define('ADMIN_EMAIL', '');
define('SENDING_EMAIL', '');



define('TIMEZONE', 'GMT');
//CONSTANTS

//The sitewide hashkey used for all passwords
//For database passwords
define('HASH_PASSWORD_KEY', 'ReplaceME');
//For other security
define('HASH_GENERAL_KEY', 'ANDreplaceME');
define('HASH_VERIFY_KEY', 'ANDreplaceMEtoo');

//For google's recaptcha form - https://developers.google.com/recaptcha/
define('RECAPTCHA_SITE_KEY', "");
define('RECAPTCHA_SECRET_KEY', "");

//For mapbox
define('MAPBOX_KEY', '');
define('VIDEOARCHIVE_KEY', '');
//For mailchimp
define('MAILCHIMP_KEY', '');

//DATABASE
define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'test');
define('DB_USER', 'root');
define('DB_PASS', 'root');

if(!DB_NAME) {
	print "No DB Name selected in config";
	exit;
}

/*
function public_base_directory() 
{ 
    //get public directory structure eg "/top/second/third" 
    $public_directory = dirname($_SERVER['PHP_SELF']); 
    //place each directory into array 
    $directory_array = explode('/', $public_directory); 
    //get highest or top level in array of directory strings 
    $public_base = max($directory_array); 
    
    return $public_base; 
} 
echo public_base_directory().'<br/>';
echo dirname(__FILE__).'<Br/>';
*/


/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *	   installation
 *     development
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT','installation');	
/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but production will hide them.
 */
if (defined('ENVIRONMENT')){

	switch (ENVIRONMENT){
		case 'installation';
	/*
		Set to 'yes' if wanting to create the databse to start with (see model lib)
		Once details / database are set up, change to anything else
	*/

		
		case 'development':
			error_reporting(E_ALL);
		break;
		case 'production':
			error_reporting(0);
		break;
		default:
			exit('The application environment is not set correctly.');
	}
}

?>
